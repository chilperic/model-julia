@parameters begin
    C16AcylCoACYT
    CarCYT
    CarMAT 
    CoACYT 
    CoAMATt
    EXTERNAL
    FADtMAT 
    K1acesink 
    K1fadhsink 
    K1nadhsink 
    Keqcact
    Keqcpt1 
    Keqcpt2 
    Keqcrot 
    Keqlcad
    Keqmcad
    Keqmckat
    Keqmschad 
    Keqmtp 
    Keqscad
    Keqvlcad
    KicactC10AcylCarCYT 
    KicactC12AcylCarCYT 
    KicactC14AcylCarCYT 
    KicactC16AcylCarCYT 
    KicactC4AcylCarCYT 
    KicactC6AcylCarCYT 
    KicactC8AcylCarCYT 
    KicactCarCYT 
    Kicpt1MalCoACYT
    KicrotC4AcetoacylCoA
    KmcactC10AcylCarCYT 
    KmcactC10AcylCarMAT 
    KmcactC12AcylCarCYT 
    KmcactC12AcylCarMAT 
    KmcactC14AcylCarCYT 
    KmcactC14AcylCarMAT 
    KmcactC16AcylCarCYT 
    KmcactC16AcylCarMAT 
    KmcactC4AcylCarCYT 
    KmcactC4AcylCarMAT 
    KmcactC6AcylCarCYT 
    KmcactC6AcylCarMAT 
    KmcactC8AcylCarCYT 
    KmcactC8AcylCarMAT 
    KmcactCarCYT 
    KmcactCarMAT 
    Kmcpt1C16AcylCarCYT
    Kmcpt1C16AcylCoACYT 
    Kmcpt1CarCYT
    Kmcpt1CoACYT 
    Kmcpt2C10AcylCarMAT 
    Kmcpt2C10AcylCoAMAT 
    Kmcpt2C12AcylCarMAT 
    Kmcpt2C12AcylCoAMAT 
    Kmcpt2C14AcylCarMAT 
    Kmcpt2C14AcylCoAMAT 
    Kmcpt2C16AcylCarMAT 
    Kmcpt2C16AcylCoAMAT 
    Kmcpt2C4AcylCarMAT 
    Kmcpt2C4AcylCoAMAT 
    Kmcpt2C6AcylCarMAT 
    Kmcpt2C6AcylCoAMAT 
    Kmcpt2C8AcylCarMAT 
    Kmcpt2C8AcylCoAMAT 
    Kmcpt2CarMAT 
    Kmcpt2CoAMAT 
    KmcrotC10EnoylCoAMAT 
    KmcrotC10HydroxyacylCoAMAT 
    KmcrotC12EnoylCoAMAT 
    KmcrotC12HydroxyacylCoAMAT 
    KmcrotC14EnoylCoAMAT 
    KmcrotC14HydroxyacylCoAMAT 
    KmcrotC16EnoylCoAMAT 
    KmcrotC16HydroxyacylCoAMAT 
    KmcrotC4EnoylCoAMAT 
    KmcrotC4HydroxyacylCoAMAT 
    KmcrotC6EnoylCoAMAT 
    KmcrotC6HydroxyacylCoAMAT 
    KmcrotC8EnoylCoAMAT 
    KmcrotC8HydroxyacylCoAMAT 
    KmlcadC10AcylCoAMAT 
    KmlcadC10EnoylCoAMAT 
    KmlcadC12AcylCoAMAT
    KmlcadC12EnoylCoAMAT 
    KmlcadC14AcylCoAMAT
    KmlcadC14EnoylCoAMAT 
    KmlcadC16AcylCoAMAT
    KmlcadC16EnoylCoAMAT 
    KmlcadC8AcylCoAMAT 
    KmlcadC8EnoylCoAMAT 
    KmlcadFAD 
    KmlcadFADH 
    KmmcadC10AcylCoAMAT
    KmmcadC10EnoylCoAMAT 
    KmmcadC12AcylCoAMAT
    KmmcadC12EnoylCoAMAT 
    KmmcadC4AcylCoAMAT 
    KmmcadC4EnoylCoAMAT 
    KmmcadC6AcylCoAMAT
    KmmcadC6EnoylCoAMAT 
    KmmcadC8AcylCoAMAT
    KmmcadC8EnoylCoAMAT 
    KmmcadFAD 
    KmmcadFADH 
    KmmckatAcetylCoAMAT 
    KmmckatC10AcylCoAMAT 
    KmmckatC10KetoacylCoAMAT
    KmmckatC12AcylCoAMAT 
    KmmckatC12KetoacylCoAMAT
    KmmckatC14AcylCoAMAT 
    KmmckatC14KetoacylCoAMAT
    KmmckatC16AcylCoAMAT 
    KmmckatC16KetoacylCoAMAT
    KmmckatC4AcetoacylCoAMAT 
    KmmckatC4AcylCoAMAT 
    KmmckatC6AcylCoAMAT 
    KmmckatC6KetoacylCoAMAT
    KmmckatC8AcylCoAMAT 
    KmmckatC8KetoacylCoAMAT
    KmmckatCoAMAT 
    KmmschadC10HydroxyacylCoAMAT
    KmmschadC10KetoacylCoAMAT
    KmmschadC12HydroxyacylCoAMAT
    KmmschadC12KetoacylCoAMAT
    KmmschadC14HydroxyacylCoAMAT
    KmmschadC14KetoacylCoAMAT
    KmmschadC16HydroxyacylCoAMAT
    KmmschadC16KetoacylCoAMAT
    KmmschadC4AcetoacylCoAMAT 
    KmmschadC4HydroxyacylCoAMAT 
    KmmschadC6HydroxyacylCoAMAT 
    KmmschadC6KetoacylCoAMAT
    KmmschadC8HydroxyacylCoAMAT 
    KmmschadC8KetoacylCoAMAT
    KmmschadNADHMAT
    KmmschadNADMAT 
    KmmtpAcetylCoAMAT 
    KmmtpC10AcylCoAMAT 
    KmmtpC10EnoylCoAMAT 
    KmmtpC12AcylCoAMAT 
    KmmtpC12EnoylCoAMAT 
    KmmtpC14AcylCoAMAT 
    KmmtpC14EnoylCoAMAT 
    KmmtpC16AcylCoAMAT 
    KmmtpC16EnoylCoAMAT 
    KmmtpC6AcylCoAMAT 
    KmmtpC8AcylCoAMAT 
    KmmtpC8EnoylCoAMAT 
    KmmtpCoAMAT 
    KmmtpNADHMAT 
    KmmtpNADMAT 
    KmscadC4AcylCoAMAT 
    KmscadC4EnoylCoAMAT 
    KmscadC6AcylCoAMAT 
    KmscadC6EnoylCoAMAT 
    KmscadFAD 
    KmscadFADH 
    KmvlcadC12AcylCoAMAT
    KmvlcadC12EnoylCoAMAT 
    KmvlcadC14AcylCoAMAT
    KmvlcadC14EnoylCoAMAT 
    KmvlcadC16AcylCoAMAT
    KmvlcadC16EnoylCoAMAT 
    KmvlcadFAD 
    KmvlcadFADH 
    Ksacesink 
    Ksfadhsink 
    Ksnadhsink 
    MalCoACYT
    NADtMAT 
    VCYT
    VMAT
    Vcpt1 
    Vcpt2 
    Vcrot
    Vfcact 
    Vlcad 
    Vmcad 
    Vmckat
    Vmschad
    Vmtp 
    Vrcact 
    Vscad 
    Vvlcad
    ncpt1
    sfcpt1C16
    sfcpt2C10 
    sfcpt2C12 
    sfcpt2C14
    sfcpt2C16 
    sfcpt2C4 
    sfcpt2C6 
    sfcpt2C8 
    sfcrotC10 
    sfcrotC12 
    sfcrotC14
    sfcrotC16 
    sfcrotC4
    sfcrotC6 
    sfcrotC8 
    sflcadC10 
    sflcadC12
    sflcadC14
    sflcadC16
    sflcadC8
    sfmcadC10
    sfmcadC12 
    sfmcadC4 
    sfmcadC6
    sfmcadC8 
    sfmckatC10 
    sfmckatC12 
    sfmckatC14
    sfmckatC16
    sfmckatC4 
    sfmckatC6
    sfmckatC8 
    sfmschadC10 
    sfmschadC12 
    sfmschadC14
    sfmschadC16
    sfmschadC4 
    sfmschadC6
    sfmschadC8 
    sfmtpC10 
    sfmtpC12 
    sfmtpC14
    sfmtpC16
    sfmtpC8 
    sfscadC4
    sfscadC6
    sfvlcadC12 
    sfvlcadC14 
    sfvlcadC16
    compartment
end

ps = [
C16AcylCoACYT => 25.0,
CarCYT => 200.0,
CarMAT => 950.0,
CoACYT => 140.0,
CoAMATt => 5000.0,
EXTERNAL => 1.0,
FADtMAT => 0.77,
K1acesink => 70.0,
K1fadhsink => 0.46,
K1nadhsink => 16.0,
Keqcact => 1.0,
Keqcpt1 => 0.45,
Keqcpt2 => 2.22,
Keqcrot => 3.13,
Keqlcad => 6.0,
Keqmcad => 6.0,
Keqmckat => 1051.0,
Keqmschad => 0.000217,
Keqmtp => 0.71,
Keqscad => 6.0,
Keqvlcad => 6.0,
KicactC10AcylCarCYT => 56.0,
KicactC12AcylCarCYT => 56.0,
KicactC14AcylCarCYT => 56.0,
KicactC16AcylCarCYT => 56.0,
KicactC4AcylCarCYT => 56.0,
KicactC6AcylCarCYT => 56.0,
KicactC8AcylCarCYT => 56.0,
KicactCarCYT => 200.0,
Kicpt1MalCoACYT => 9.1,
KicrotC4AcetoacylCoA => 1.6,
KmcactC10AcylCarCYT => 15.0,
KmcactC10AcylCarMAT => 15.0,
KmcactC12AcylCarCYT => 15.0,
KmcactC12AcylCarMAT => 15.0,
KmcactC14AcylCarCYT => 15.0,
KmcactC14AcylCarMAT => 15.0,
KmcactC16AcylCarCYT => 15.0,
KmcactC16AcylCarMAT => 15.0,
KmcactC4AcylCarCYT => 15.0,
KmcactC4AcylCarMAT => 15.0,
KmcactC6AcylCarCYT => 15.0,
KmcactC6AcylCarMAT => 15.0,
KmcactC8AcylCarCYT => 15.0,
KmcactC8AcylCarMAT => 15.0,
KmcactCarCYT => 130.0,
KmcactCarMAT => 130.0,
Kmcpt1C16AcylCarCYT => 136.0,
Kmcpt1C16AcylCoACYT => 13.8,
Kmcpt1CarCYT => 250.0,
Kmcpt1CoACYT => 40.7,
Kmcpt2C10AcylCarMAT => 51.0,
Kmcpt2C10AcylCoAMAT => 38.0,
Kmcpt2C12AcylCarMAT => 51.0,
Kmcpt2C12AcylCoAMAT => 38.0,
Kmcpt2C14AcylCarMAT => 51.0,
Kmcpt2C14AcylCoAMAT => 38.0,
Kmcpt2C16AcylCarMAT => 51.0,
Kmcpt2C16AcylCoAMAT => 38.0,
Kmcpt2C4AcylCarMAT => 51.0,
Kmcpt2C4AcylCoAMAT => 1000000.0,
Kmcpt2C6AcylCarMAT => 51.0,
Kmcpt2C6AcylCoAMAT => 1000.0,
Kmcpt2C8AcylCarMAT => 51.0,
Kmcpt2C8AcylCoAMAT => 38.0,
Kmcpt2CarMAT => 350.0,
Kmcpt2CoAMAT => 30.0,
KmcrotC10EnoylCoAMAT => 25.0,
KmcrotC10HydroxyacylCoAMAT => 45.0,
KmcrotC12EnoylCoAMAT => 25.0,
KmcrotC12HydroxyacylCoAMAT => 45.0,
KmcrotC14EnoylCoAMAT => 100.0,
KmcrotC14HydroxyacylCoAMAT => 45.0,
KmcrotC16EnoylCoAMAT => 150.0,
KmcrotC16HydroxyacylCoAMAT => 45.0,
KmcrotC4EnoylCoAMAT => 40.0,
KmcrotC4HydroxyacylCoAMAT => 45.0,
KmcrotC6EnoylCoAMAT => 25.0,
KmcrotC6HydroxyacylCoAMAT => 45.0,
KmcrotC8EnoylCoAMAT => 25.0,
KmcrotC8HydroxyacylCoAMAT => 45.0,
KmlcadC10AcylCoAMAT => 24.3,
KmlcadC10EnoylCoAMAT => 1.08,
KmlcadC12AcylCoAMAT => 9.0,
KmlcadC12EnoylCoAMAT => 1.08,
KmlcadC14AcylCoAMAT => 7.4,
KmlcadC14EnoylCoAMAT => 1.08,
KmlcadC16AcylCoAMAT => 2.5,
KmlcadC16EnoylCoAMAT => 1.08,
KmlcadC8AcylCoAMAT => 123.0,
KmlcadC8EnoylCoAMAT => 1.08,
KmlcadFAD => 0.12,
KmlcadFADH => 24.2,
KmmcadC10AcylCoAMAT => 5.4,
KmmcadC10EnoylCoAMAT => 1.08,
KmmcadC12AcylCoAMAT => 5.7,
KmmcadC12EnoylCoAMAT => 1.08,
KmmcadC4AcylCoAMAT => 135.0,
KmmcadC4EnoylCoAMAT => 1.08,
KmmcadC6AcylCoAMAT => 9.4,
KmmcadC6EnoylCoAMAT => 1.08,
KmmcadC8AcylCoAMAT => 4.0,
KmmcadC8EnoylCoAMAT => 1.08,
KmmcadFAD => 0.12,
KmmcadFADH => 24.2,
KmmckatAcetylCoAMAT => 30.0,
KmmckatC10AcylCoAMAT => 13.83,
KmmckatC10KetoacylCoAMAT => 2.1,
KmmckatC12AcylCoAMAT => 13.83,
KmmckatC12KetoacylCoAMAT => 1.3,
KmmckatC14AcylCoAMAT => 13.83,
KmmckatC14KetoacylCoAMAT => 1.2,
KmmckatC16AcylCoAMAT => 13.83,
KmmckatC16KetoacylCoAMAT => 1.1,
KmmckatC4AcetoacylCoAMAT => 12.4,
KmmckatC4AcylCoAMAT => 13.83,
KmmckatC6AcylCoAMAT => 13.83,
KmmckatC6KetoacylCoAMAT => 6.7,
KmmckatC8AcylCoAMAT => 13.83,
KmmckatC8KetoacylCoAMAT => 3.2,
KmmckatCoAMAT => 26.6,
KmmschadC10HydroxyacylCoAMAT => 8.8,
KmmschadC10KetoacylCoAMAT => 2.3,
KmmschadC12HydroxyacylCoAMAT => 3.7,
KmmschadC12KetoacylCoAMAT => 1.6,
KmmschadC14HydroxyacylCoAMAT => 1.8,
KmmschadC14KetoacylCoAMAT => 1.4,
KmmschadC16HydroxyacylCoAMAT => 1.5,
KmmschadC16KetoacylCoAMAT => 1.4,
KmmschadC4AcetoacylCoAMAT => 16.9,
KmmschadC4HydroxyacylCoAMAT => 69.9,
KmmschadC6HydroxyacylCoAMAT => 28.6,
KmmschadC6KetoacylCoAMAT => 5.8,
KmmschadC8HydroxyacylCoAMAT => 16.3,
KmmschadC8KetoacylCoAMAT => 4.1,
KmmschadNADHMAT => 5.4,
KmmschadNADMAT => 58.5,
KmmtpAcetylCoAMAT => 30.0,
KmmtpC10AcylCoAMAT => 13.83,
KmmtpC10EnoylCoAMAT => 25.0,
KmmtpC12AcylCoAMAT => 13.83,
KmmtpC12EnoylCoAMAT => 25.0,
KmmtpC14AcylCoAMAT => 13.83,
KmmtpC14EnoylCoAMAT => 25.0,
KmmtpC16AcylCoAMAT => 13.83,
KmmtpC16EnoylCoAMAT => 25.0,
KmmtpC6AcylCoAMAT => 13.83,
KmmtpC8AcylCoAMAT => 13.83,
KmmtpC8EnoylCoAMAT => 25.0,
KmmtpCoAMAT => 30.0,
KmmtpNADHMAT => 50.0,
KmmtpNADMAT => 60.0,
KmscadC4AcylCoAMAT => 10.7,
KmscadC4EnoylCoAMAT => 1.08,
KmscadC6AcylCoAMAT => 285.0,
KmscadC6EnoylCoAMAT => 1.08,
KmscadFAD => 0.12,
KmscadFADH => 24.2,
KmvlcadC12AcylCoAMAT => 2.7,
KmvlcadC12EnoylCoAMAT => 1.08,
KmvlcadC14AcylCoAMAT => 4.0,
KmvlcadC14EnoylCoAMAT => 1.08,
KmvlcadC16AcylCoAMAT => 6.5,
KmvlcadC16EnoylCoAMAT => 1.08,
KmvlcadFAD => 0.12,
KmvlcadFADH => 24.2,
Ksacesink => 6000000.0,
Ksfadhsink => 6000000.0,
Ksnadhsink => 6000000.0,
MalCoACYT => 0.0,
NADtMAT => 250.0,
VCYT => 2.2^-6,
VMAT => 1.8^-6,
Vcpt1 => 0.012,
Vcpt2 => 0.391,
Vcrot => 3.6,
Vfcact => 0.42,
Vlcad => 0.01,
Vmcad => 0.081,
Vmckat => 0.377,
Vmschad => 1.0,
Vmtp => 2.84,
Vrcact => 0.42,
Vscad => 0.081,
Vvlcad => 0.008,
ncpt1 => 2.4799,
sfcpt1C16 => 1.0,
sfcpt2C10 => 0.95,
sfcpt2C12 => 0.95,
sfcpt2C14 => 1.0,
sfcpt2C16 => 0.85,
sfcpt2C4 => 0.01,
sfcpt2C6 => 0.15,
sfcpt2C8 => 0.35,
sfcrotC10 => 0.33,
sfcrotC12 => 0.25,
sfcrotC14 => 0.2,
sfcrotC16 => 0.13,
sfcrotC4 => 1.0,
sfcrotC6 => 0.83,
sfcrotC8 => 0.58,
sflcadC10 => 0.75,
sflcadC12 => 0.9,
sflcadC14 => 1.0,
sflcadC16 => 0.9,
sflcadC8 => 0.4,
sfmcadC10 => 0.8,
sfmcadC12 => 0.38,
sfmcadC4 => 0.12,
sfmcadC6 => 1.0,
sfmcadC8 => 0.87,
sfmckatC10 => 0.65,
sfmckatC12 => 0.38,
sfmckatC14 => 0.2,
sfmckatC16 => 0.0,
sfmckatC4 => 0.49,
sfmckatC6 => 1.0,
sfmckatC8 => 0.81,
sfmschadC10 => 0.64,
sfmschadC12 => 0.43,
sfmschadC14 => 0.5,
sfmschadC16 => 0.6,
sfmschadC4 => 0.67,
sfmschadC6 => 1.0,
sfmschadC8 => 0.89,
sfmtpC10 => 0.73,
sfmtpC12 => 0.81,
sfmtpC14 => 0.9,
sfmtpC16 => 1.0,
sfmtpC8 => 0.34,
sfscadC4 => 1.0,
sfscadC6 => 0.3,
sfvlcadC12 => 0.11,
sfvlcadC14 => 0.42,
sfvlcadC16 => 1.0,
compartment => 1.0
]
