using DifferentialEquations
using ModelingToolkit
using Plots


include("params.jl")
include("variables.jl")
include("init_vals.jl")
D = Differential(t)
include("ode_sys.jl")

@named sys = ODESystem(ode_sys)
ss = structural_simplify(sys)

tspan = (0.0, 100.0)
prob = ODEProblem(ss,u0,tspan,ps)
sol = solve(prob, Rodas5())

plot(sol)
